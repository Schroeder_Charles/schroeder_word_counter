
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.*;
/**
 This class handles every aspect of the GUI. It includes a file chooser that will pass the chosen
 file to the WordCounterHandler class. The Main class sets this window to be visible.
 */
public class TempWindow extends JFrame
 {
	//defining Jwindow parameters
	JButton openButton = new JButton("Click Here to Open a File");
	JButton cancelButton = new JButton("Cancel");
	static JTextArea textarea = new JTextArea();
	JScrollPane outputfield = new JScrollPane(textarea);

	/**
	 This function simply defines the properties of the GUI window.
	 */
	public TempWindow() {
		
		//general window properties
		this.setTitle("Word Counter");
		this.setBounds(200, 200, 600, 400);
		
		//setting layout manager to fully manual
		this.getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//textfield settings
		this.getContentPane().add(outputfield);
		this.outputfield.setBounds(30, 30, 500, 300);
		this.textarea.setBounds(30, 30, 500, 300);
		this.textarea.setEditable(false);
		this.textarea.append("Please choose a file." + "\n");
		this.textarea.append("Each word and its number of uses will appear here:" + "\n");

		//buttons
		this.openButton.setBounds(25, 338, 200, 30);
		this.getContentPane().add(openButton);
		this.openButton.addActionListener( new ButtonListener());
        
		
	}

    /**
    This class and function handle user input on the buttons in the GUI. The ActionEvent of
    pressing the button is passed in, and the chosen file is checked, and if it is a text file,
    approved. This file is then passed to the WordCounterHandler class.
    @param ActionEvent
     */
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed( ActionEvent e) {
            
        	//creating filechooser, setting acceptable file types
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            
            int chooserSuccess = chooser.showOpenDialog( null);
         
            if( chooserSuccess == JFileChooser.APPROVE_OPTION) 
            {
                File chosenFile = chooser.getSelectedFile();
                // Pass this file to WordCounterHandler
                try {
					WordCounterHandler.wordCounter(chosenFile);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                
            }
            else 
            {
            		//do nothing
            }
        }
    }
    /**
     This function is passed a the current word (String) and the number of appearances of that
     word (integer). It converts the integer to a String, then outputs both Strings to the GUI, or
     more specifically, the textarea.
     @param text
     @param number
     */
    public static void writeToGUI(String text, int number)
    {
    	//outputs the word and its count to the GUI
    	String stringNumber = Integer.toString(number);
       textarea.append(text + " " + stringNumber + "\n");
    }
	
}