import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 This class counts the number of words in the file, adds each unique word to a list, then counts
 how many times each word in the list appears in the file. First, a new scanner object is created 
 to manage the file the user chose. Second, the number of words are counted. Third, each word in the
 file is sent through a function to remove any and all symbols. This word is checked against the other
 words in the current list. If it is not already in the list, it is added to the list. Next, each word
 in the list is compared all the words in the file, and the number of appearances of that word is tallied.
 The word and its count are then immediately output to the GUI. 
 */
public class WordCounterHandler {
	/**
	 This function is described in depth in the class description above. It takes in a text file,
	 and will output to the class TempWindow, which will write to the GUI.
	 @param chosenFile
	 @throws IOException
	 */
	public static void wordCounter(File chosenFile) throws IOException
	{	
		Scanner sc = new Scanner(chosenFile);
		
	int numberOfWords = 0;
	int currentWordCount = 0;
	String currentWord;
	List<String> wordList = new ArrayList<String>();
	
	//grabbing the total number of words from the document
	while(sc.hasNext())
	{
		numberOfWords++;
		sc.next();
	}
		
		//System.out.println(numberOfWords);
		
		//resets the scanner to the beginning, next line reinitializes the file
		sc.reset();
		sc = new Scanner(chosenFile);
		
			//add all unique words to our list
		for(int q =0; q < numberOfWords; q++)
		{	
			currentWord = sc.next();
			
			String amendedWord = removePunctuation(currentWord);
			
			assignToList(amendedWord, wordList);
		}
		//the previous for loop adds all the unique words
		//but, it leaves our scanner iterator at the end of the list
		sc.reset();
		sc = new Scanner(chosenFile);
		//we need a new object so we can iterate through our list like an array
		Object[] wordListArray = new String[numberOfWords];
		wordList.toArray(wordListArray);
		
		//these nested loops compares the words in our list to the whole document
		//then it sends the word and the count to the output to GUI function
		for(int i =0; i < wordList.size(); i++)
		{
			while(sc.hasNext())
			{
				currentWord = sc.next();
				String amendedWord = removePunctuation(currentWord);
				//compare the current word to all the words in the file and count it
				if(((String) wordListArray[i]).equalsIgnoreCase(amendedWord))
				{
					currentWordCount++;
				}
			}
			
			outputToGUI(wordListArray[i], currentWordCount);
			sc.reset();
			sc = new Scanner(chosenFile);
			currentWordCount = 0;
		
		}
		
		sc.close();
	}

	/**
	 This function is handed the current word and its count. It casts the word Object as a String,
	 and passes it and the integer that represents the number of times that word appears, to the
	 TempWindow class that handles the GUI.
	 @param whatWord
	 @param howMany
	 */
	public static void outputToGUI(Object whatWord, int howMany)
	{
		//System.out.println(whatWord + " " + howMany);
		TempWindow.writeToGUI((String)whatWord, howMany);
	} 
	/**
	 This function is passed in the current word, after it has been removed of symbols, and adds it
	 to the list of all unique words. 
	 @param amendedWord
	 @param wordList
	 */
	public static void assignToList(String amendedWord,List<String> wordList)
	{
		if(wordList.contains(amendedWord))
		{
			//if already in list do nothing
		} else
		{	//add to list
			wordList.add(amendedWord);
		}
	}
	/**
	 This function is passed a word in String form and removes symbols in that word. First, the number of 
	 symbols in the word is determined. Next, the word is parsed through, and all the letters are sent to 
	 a character array. All non-letters are ignored. Finally, a string is assigned the new character array
	 and returned to the caller. 
	 @param currentWord
	 @return amendedWord
	 */
	public static String removePunctuation(String currentWord)
	{
		int numberOfSymbols = 0;
		//find the number of none letters in the string
		for(int q=0; q<currentWord.length(); q++)
		{
			if(Character.isLetter(currentWord.charAt(q)) == false)
			{
				numberOfSymbols++;
			}
		}
		
		//create an array of characters that is only the length of the number of letters
		char [] charArray = new char [currentWord.length()-numberOfSymbols];
		int index =0;
		//iterate through and only add the letters
		for(int r=0; r < currentWord.length(); r++)
		{
			if(Character.isLetter(currentWord.charAt(r)))
			{
				charArray[index] = currentWord.charAt(r);
				index++;
			} else
			{
				//do nothing
			}
		}
		
		String amendedWord = new String(charArray);
		
		return amendedWord;
	}
	
}
