/**
 This program reads a file and counts the number of times each unique word appears.
 The Main class simply sets the GUI window to display.
 */
public class Main {
	
	public static void main(String[] args) {
		
		//Building UI, setting default visibility
		TempWindow userFileSelectorWindow = new TempWindow ();
		userFileSelectorWindow.setVisible(true);
	}

}